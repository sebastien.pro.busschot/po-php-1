<?php
trait races
{
    public function race($race)
    {
        switch ($race) {
            case "homme":
                $this->_pv = 120;
                $this->_force = 16;
                $this->_endurance = 3;
                $this->_race = $race;
                break;

            case "nain":
                $this->_pv = 110;
                $this->_force = 28;
                $this->_endurance = 3;
                $this->_race = $race;
                break;

            case "elfe":
                $this->_pv = 90;
                $this->_force = 15;
                $this->_endurance = 4;
                $this->_race = $race;
                break;

            case "orc":
                $this->_pv = 90;
                $this->_force = 10;
                $this->_endurance = 3;
                $this->_race = $race;
                break;
        }
    }
}

?>