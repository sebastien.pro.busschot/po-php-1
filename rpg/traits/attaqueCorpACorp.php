<?php
    trait attaqueCorpACorp
    {
        // Phase 1 : attaqué.pv - assaillant.force ;
        // Phase 2 : attaqué.pv > 0 ? assaillant.pv - attaqué.force : echo "il est mort", rm objet ;
        public function corpACorps($victime)
        {
            // si la cible est en vie je l'attaque
            if($victime->_pv > 0){
                $degatsAgresseur = $this->_force + rand(-5,5);
                $victime->_pv = $victime->_pv - $degatsAgresseur;
                echo "\n" . $this->_nom . " attaque " . $victime->_nom . " et lui inflige " . $degatsAgresseur . " dégats.\n";
            } else {
                echo "\n".$this->_nom . " a bien vu que " . $victime->_nom . " est à terre mais il en profite pour lui donner un dernier coup gratuit ! Quelle indignité !\n";
            }

            // si la cible n'est pas morte elle se rebiffe
            if($victime->_pv > 0){
                if($this->_pv > 0){
                    $degatsVictime = $victime->_force + rand(-5,5);
                    $this->_pv = $victime->_pv - $degatsVictime;
                    echo $victime->_nom . " n'a pas aimé recevoir un coup et contre-attaque. " . $this->_nom . " se prend un coup en retour";
                    if($this->_pv > 0){
                        echo  " et n'a plus que " . $this->_pv . " pv.\n";
                    } else {
                        echo " et meurt... RIP\n";
                    }
                }
            } else {
                echo "Aie ça fait mal, " . $victime->_nom . " n'a pas l'air bien.\nEffectivement " . $victime->_nom . "  est mort. Personne ne le regrettera.\n";
            }
        }
    }
?>