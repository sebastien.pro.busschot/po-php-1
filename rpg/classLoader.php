<?php

spl_autoload_register(function ($class) {
    //var_dump($class);
    if (file_exists('./traits/' . $class . '.php')) :
        include('./traits/' . $class . '.php');
    elseif (file_exists('./interfaces/' . $class . '.php')) :
        include('./interfaces/' . $class . '.php');
    elseif (file_exists('./class/' . $class . '.php')) :
        include('./class/' . $class . '.php');
    endif;
});
