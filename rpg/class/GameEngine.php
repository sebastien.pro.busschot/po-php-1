<?php
class GameEngine {
    // tableau personnages
    public $_heros = [];
    // tableau PNJ
    public $_personnagesNonJoueur = [];

    public function nouveauPerso($race, $nom, $equipe){
        if($race == NULL){
            while(true){
                echo "Equipe 1 : Les héros, froussarts et peu dégourdis mais on les aime quand même.\n";
                echo "Equipe 2 : Les méchants, enfin...question de point de vue...\n";
                $equipe = readline("Veux-tu ajouter un Héro ou un PNJ? => 1 ou 2");
                if($equipe == 1 || $equipe == 2){
                    break;
                }

                $race = readline("De quelle race va être la recrue?");
                $nom = readline("Comment va s'appeler le nouvel arivant?");
            }
        }

        if($equipe == 1){
            array_push($this->_heros, new Personnage($race, $nom));
        } else if($equipe == 2){
            array_push($this->_personnagesNonJoueur, new Personnage($race, $nom));
        } else {
            echo "Erreur dans la création du personnage.";
        }
        
        
    }

    public function batailleRangee() {
        //var_dump($this->_heros);
        $tour = 0;
        $gameover = false;
        while($gameover == false){
            // les deux equipes s'ataquent jusqu'a l'éradication de l'une d'entre elles
            // a chaque tour choisir au hazard un combatant
            $nombreHeros = count($this->_heros)-1;
            $nombreAntagonistes = count($this->_personnagesNonJoueur)-1;
            $hero = mt_rand(0,$nombreHeros) ;
            $pnj = mt_rand(0,$nombreAntagonistes);
            //var_dump($this->_heros[$hero]);

            // n'autoriser l'attaque que si l'adversaire est vivant // interdire l'attaque unilatérale
            if($this->_heros[$hero]->_pv > 0 && $this->_personnagesNonJoueur[$pnj]->_pv > 0){
                $tour++;
                // qui attaque le premier
                $quiAttaque = mt_rand(1,2);
                if($quiAttaque == 1){
                    $this->_heros[$hero]->corpACorps($this->_personnagesNonJoueur[$pnj]);
                    $this->_personnagesNonJoueur[$pnj]->special($this->_heros[$hero],$this->_personnagesNonJoueur[$pnj],$this);
                    $this->_heros[$hero]->special($this->_personnagesNonJoueur[$pnj],$this->_heros[$hero],$this);
                } else if($quiAttaque == 2){
                    $this->_personnagesNonJoueur[$pnj]->corpACorps($this->_heros[$hero]);
                    $this->_personnagesNonJoueur[$pnj]->special($this->_heros[$hero],$this->_personnagesNonJoueur[$pnj],$this);
                    $this->_heros[$hero]->special($this->_personnagesNonJoueur[$pnj],$this->_heros[$hero],$this);
                }
            }
            //var_dump($this->_heros);
            //qui a survécu
            $survivors = [];
            $teamHero = false;
            $teamPNJ = false;
            for($i = 0; $i < count($this->_heros); $i++){
                if($this->_heros[$i]->_pv > 0){
                    $teamHero = true;
                    array_push($survivors, $this->_heros[$i]);
                }
            }
            for($i = 0; $i < count($this->_personnagesNonJoueur); $i++){
                if($this->_personnagesNonJoueur[$i]->_pv > 0){
                    $teamPNJ = true;
                    array_push($survivors, $this->_personnagesNonJoueur[$i]);
                }
            }

            // une equipe est décimée on stoppe
            if($teamHero == false || $teamPNJ == false){
                echo("\n\n////////////////////////////////  RÉSULTATS DU COMBAT   ////////////////////////////////\n\n");                
                echo("\nUne equipe vient de remporter une victoire en ".$tour." tours. Non sans peine, glorifiez nos vainqueurs !\n");
                for($i = 0; $i < count($survivors); $i++){
                    echo($survivors[$i]->_nom." : ".$survivors[$i]->_pv." pv restants\n");
                }
                
                $this->_heros = [];
                $this->_personnagesNonJoueur = [];
                return $teamHero == false ? 2 : 1 ;
            }
            
            
            if($tour==1000){
                echo("Boucle infinie, arrêt du programme.");
                break;
            }

            // $tour++;
            // // tous les heros sont morts
            // foreach($this->_heros as $hero){
            //     //var_dump($hero);
            //     $gameover = $hero->_pv > 0 ? false : true ;
            //     if($hero->_pv > 0){
            //         $gameover = false;
            //     } else {
            //         echo $hero->_nom . " est mort au tour précédent. Paix à son âme...s'il en avait une.\n";
            //     }
            // }


            // $combatOver = false;
            // while($combatOver == false){
            //     $combatOver = true;
            //     $countHeros = count($this->_heros);
            //     // parcourir $heros[]
            //     for($i = 0 ; $i < $countHeros ; $i++){
            //         if($this->_heros[$i]->_pv > 0){
            //             // pour attaquer $pnj[]
            //             $countPNJ = count($this->_personnagesNonJoueur);
            //             for($j = 0 ; $j < $countPNJ ; $j++) {
            //                 if($this->_personnagesNonJoueur[$j]->_pv > 0 && $this->_heros[$i]->_pv > 0){
            //                     $this->_personnagesNonJoueur = $this->_personnagesNonJoueur[$j]->special($hero, $this->_personnagesNonJoueur, $this);
            //                     // $personnagesNonJoueur = special($hero, $personnagesNonJoueur); ne pas oublie le return ds la fonction
            //                     //$this->_heros[$i]->corpACorps($this->_personnagesNonJoueur[$j]);
            //                 }
            //             }
            //         }
            //     }
            // }
        }
    }

}
