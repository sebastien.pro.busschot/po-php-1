<?php

    class Personnage implements Ipersonnage {
        public $_pv;
        protected $_force;
        protected $_endurance;
        public $_nom;
        protected $_race;

        use races;
        use attaqueCorpACorp;

        public function __construct($race, $nom){
            $this->_nom = $nom;
            $this->race($race);
        }

        public function special($victime, $assaillant, $object){
            //var_dump($teamArray);
            switch($this->_race){
                case "homme":
                if(mt_rand(0,1) == 0){
                    if($this->_pv > 0){
                        $regenPV = mt_rand(120-$this->_pv, 120);
                        echo("\n".$this->_nom." se cache le temps de reprendre son soufle, cela lui permet de remonter ses PVs. Désormais ".$this->_nom." a ".$this->_pv." PVs.\n");
                    } else {
                        echo("\n".$this->_nom." est tombé. Mais il n'a pas dit son dernier mot, il se relève, sait qu'il va surement mourir, mais il tient bon !");
                        $this->_pv = 1;
                    }
                }
                break;

            case "nain":
                if($this->_pv > 0){
                    if(mt_rand(0,4) == 0){
                        $berserk = $assaillant->_force * mt_rand(1,4);
                        echo("\n".$assaillant->_nom." est furieux ! De rage, il frappe son adversaire de toutes ses forces mais sa rage l'aveugle.\nIl frappe si fort et si vite qu'on n'a pas la moindre idée de ce qui se passe mais on est sur d'une chose, ".$victime->_nom." déguste.\n");
                        //echo $berserk;
                        $victime->_pv = $victime->_pv - $berserk;
                    }
                }
                break;

            case "elfe":
                if($this->_pv > 0){
                    $nombreFleches = mt_rand(0,20);
                    $victime->_pv = $victime->_pv - ($nombreFleches * 1);
                    echo("\n".$this->_nom." a fait pleuvoir une pluie de fleches sur ".$victime->_nom." il n'a pas pu toutes les éviter. On dirait un porc épic !\n");
                }
                break;

            case "orc":
                // s'il n'est pas mort 10% chance de se cloner
                if($this->_pv > 0){
                    $seed = rand(0,3);
                    if($seed == 0){
                        $cloneName = "Sbire ".$this->_race;
                        //echo $cloneName;
                        //array_push($teamArray, new Personnage($this->_race, $cloneName));
                        echo("\nLâche ! ".$assaillant->_nom.", pris de panique face à la force de ".$victime->_nom." vient d'invoquer un sbire...\n".$victime->_nom." et son equipe ont du souci à se faire mais leur courage tient bon !\n");
                        $object->nouveauPerso($this->_race, $cloneName, 2);
                        return $object;
                    }
                }

                break;
            }
        }

    }



    
?>
