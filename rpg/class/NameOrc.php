<?php
    class NameOrc extends Name {

        protected $_namesOrcM = [Agronak, Atulg, Azuk, Bagamul, Bakh, Baronk, Bashag, Bazgulub, Bazur, Bogakh, Bogrum, Borug, Both, Brag, Brokil, Brugo, Bugak, Bugdul, Bugharz, Bugrash, Bugrol, Dubok, Bumbub, Burul, Burz, Burzak, Dul, Dulfish, Dular, Duluk, Duma, Dumbuk, Dumburz, Dur, Durbul, Durgash, Durz, Durzol, Durzub, Durzum, Dushnamub, Gadba, Ghamborz, Ghamonk, Garothmuk, Garzonk, Gashna, Gaturn, Ghoragdush, Ghorlorz, Glush, Gluthush, Goramalg, Gogron, Gorgo, Grat, Guarg, Gul, Gurak, Gurgozod, Graklak, Graman, Hanz, Khadba, Khagra, Kharag, Khargol, Koffutto, Krognak, Kurdan, Kurz, Largakh, Lob, Lorbumol, Lorzub, Lugdum, Lugrub, Lum, Lumdum, Lurog, Luronk, Magra, Magub, Mahk, Maknok, Mash, Matuk, Mauhul, Mazorn, Moghakh, Mol, Morbash, Mug, Mugdul, Muk, Murag, Murkub, Murzol, Muzgonk, Nag, Nar, Nash, Ogol, Ogrul, Ogrumbu, Olfin, Olumba, Orakh, Orok, Ozor, Ragron, Rogdul, Rugdumph, Shagol, Shakh, Shamar, Shamob, Shargam, Sharkub, Shat, Shobob, Shulong, Shum, Shura, Shurkul, Shuzug, Snaglak, Snakha, Snat, Sorogth, Ugdumph, Ughash, Ulam, Ulmug, Umug, Uram, Urbul, Urim, Urul, Urzog, Ushamph, Uzul, Wodwórg, Yadba, Yagak, Yak, Yam, Yambagorn, Yambul, Yargol, Yashnarz, Yatur];
        protected $_namesOrcF = [Agrob, Badbog, Bashuk, Batul, Bogdub, Bor, Borba, Bugdurash, Bula, Bulak, Bulfim, Bum, Bumph, Burzob, Burub, Dulug, Dura, Durgat, Durz, Garakh, Gashnakh, Ghak, Ghob, Glasha, Glob, Gluronk, Gonk, Grat, Grazob, Gul, Gulfim, Gurhul, Homraz, Kharzug, Kepkajna, Lagakh, Lambug, Largash, Lazgar, Mazoga, Mog, Mogak, Mogdurz, Mor, Morn, Murob, Murzush, Nargol, Oghash, Rogbut, Rolfish, Rogmesh, Orbul, Ragash, Rulfim, Shadbak, Shagar, Shagdub, Sharn, Sharog, Shazgob, Shelur, Snak, Ugak, Uloth, Ulumpha, Umog, Urbutha, Urog, Urzoth, Urzul, Ushat, Ushug, Voltha, Yatul, Yazgash];
        protected $_namesOrcP = [Agadbu, Aglakh, Agum, Atumph, Azorku, Badbu, Bagdub, Bagrat, Bagul, Bamog, Bar, Bargamph, Bargol, Baroth, Bash, Bashnag, Bat, Batul, Bharg, Boga, Bogamakh, Bogharz, Bogla, Boglar, Bogrol, Boguk, Bol, Bolak, Bolar, Borbog, Borbul, Brok, Bug, Bugarn, Buglump, Bulag, Bularz, Bulfim, Bulfish, Bumph, Bura, Burbog, Burbug, Burish, Burol, Buzga, Coblug, Cromgog, Dragol, Dugul, Dul, Dula, Dulob, Dumul, Dumulg, Durga, Durog, Durug, Dush, Galash, Gamorn, Gharz, Gash, Gashel, Gat, Ghash, Ghasharzol, Gholfim, Gholob, Ghorak, Glorzuf, Ghoth, Gluk, Glurkub, Glurzog, Golpok, Gonk, Gor, Gorzog, Grambak, Gro, Grulam, Gruzgob, Gulfim, Gurakh, Gurub, Hubrag, Kashug, Khagdum, Kharbush, Khar, Kharz, Khash, Khashnar, Khatub, Khazor, Lag, Lagdub, Largum, Lash, Lazgarn, Loghash, Logob, Logrob, Lorga, Lumbuk, Lumob, Lurkul, Lurn, Luzgan, Magar, Magrish, Magul, Malog, Mar, Marad, Marob, Mashnar, Minfang, Mogduk, Moghakh, Morgrump, Mughol, Muk, Mulakh, Murgak, Murgol, Murug, Murz, Muzgob, Muzgub, Muzgur, Naybek, Ogar, Ogdub, Ogdum, Olor, Olurba, Orbuma, Orkulg, Orum, Othmurga, Rimph, Rugdush, Rugob, Rush, Rushub, Shadbuk, Shagdub, Shagdulg, Shagk, Shagrak, Shagramph, Shak, Sham, Shamub, Sharbag, Sharga, Sharob, Sharolg, Shat, Shatub, Shatur, Shazog, Shug, Shugarz, Shugham, Shula, Shulor, Shumba, Shura, Shurgak, Shuzgub, Skandar, Snagarz, Snagdu, Ufthamph, Uftharz, Ugdub, Ugruma, Ular, Ulfimph, Ulfish, Urgak, Ushar, Ushug, Ushul, Uzgash, Uzgurn, Uzuk, Yagarz, Yak, Yamwort, Yargul, Yarug, Yarzol];

        protected function getAName(){
            $genre = mt_rand(1,2);
            $parent = mt_rand(0,count($this->_namesOrcP-1));
            if($genre == 1){
                $choixNom = mt_rand(0, count($this->_namesOrcM)-1);
                return $this->_namesOrcM[$choixNom]." gro-".$this->_namesOrcP[$parent];
            } else {
                $choixNom = mt_rand(0, count($this->_namesOrcF)-1);
                return $this->_namesOrcF[$choixNom]." gra-".$this->_namesOrcP[$parent];
            }
            
        }
    }

?>