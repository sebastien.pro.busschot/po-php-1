<?php

    interface Ipersonnage {
        public function __construct($race, $nom);
        public function race($race);
        public function corpACorps($victime);
        public function special($victime, $assaillant, $object);
    }

?>