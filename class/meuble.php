<?php
    include 'matiere.php';
    include 'pieds.php';

    class Meuble{
        public $_listeDePieds = [];
        public $_matiere;

        public function __construct($nombreDePieds, $hauteur, $couleur, $nomMatiere)
        {
            for($i = 0; $i < $nombreDePieds; $i++){
                $this->_listeDePieds[$i] = new pieds($hauteur);
            }
            //var_dump($this->_listeDePieds);
            $this->_matiere = new matiere($couleur, $nomMatiere);
        }

        public function toString(){
            
        echo("J'ai " . count($this->_listeDePieds) . " pieds de " . $this->_listeDePieds[1]->_hauteur . " centimètres et je suis fait de " . $this->_matiere->_nom . " " . $this->_matiere->_couleur . ".\n");
        }
    }

    $tableRonde = new Meuble(4,"50","blanc","chêne");
    $tableRonde->toString();
    // echo($tableRonde->_listeDePieds[1]->_hauteur);
    $canapeDangle = new Meuble(10, "10", "gris", "tissu");
    $canapeDangle->toString();
?>