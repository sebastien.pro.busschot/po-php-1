<?php

    class Eleve {
        public $_nom;
        public $_prenom;

        public function __construct($nom, $prenom){
            $this->_nom = $nom;
            $this->_prenom = $prenom;
        }

        public function afficheEleve(){
            echo($this->_nom ." ". $this->_prenom . "\n");
        }


    }

    $eleveSeb = new Eleve("Sebastien", "Busschot");
    $eleveSeb->afficheEleve();

    $manuInClass = new Eleve("Guérin", "Manuella");
    $manuInClass->afficheEleve();
    // $manuInClass->prenom = $donnesManu[1];
    // $manuInClass->nom = $donnesManu[0];


    $nouvelEleve = new Eleve("Dupont", "Jean");
    $nouvelEleve->afficheEleve();



?>